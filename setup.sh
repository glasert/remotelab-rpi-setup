#!/bin/sh

# ToDo:
#	- get new PiCam	
#	- Setup Camera
#	- Fix mjpeg-streamer



# getting all the input right away
	echo "Please enter base username for vnc accounts:"
	read vncBaseName
	echo "Please enter base password for vnc accounts:"
	read vncBasepasswd
	echo "Please enter the number of vnc accounts:"
	read vncCount


# update
	echo "Updating OS, this may take a while."
	sudo apt -qq update
	sudo apt -qq upgrade -y -qq

# Desktop Environment
	echo "Installing Packages:"
	echo " Desktop Environment"
	sudo apt -qq install -qq -y lxde-core lxappearance
	echo " tightvnc."
	sudo apt -qq install -qq -y tightvncserver
	echo " Arduino IDE."
	sudo apt -qq install -qq -y arduino
	echo " letsgoING ArduinoControl"
	sudo apt -qq install -qq -y python3-pip python3-tk python3-pil python3-pil.imagetk
	sudo -H python3 -m pip install -q letsgoing-rpi-arduinocontrol


# create vnc users
	echo "Creating VNC Users"
	for i in $(seq 1 $vncCount)
	do
		echo " Creating $vncBaseName$i"
		sudo adduser --gecos --quiet --disabled-login --disabled-password $vncBaseName$i > /dev/null
		sudo usermod -a -G i2c,dialout $vncBaseName$i
		
		# VNC Password:
		myuser=$vncBaseName$i
		mypasswd=$vncBasepasswd$i

		sudo mkdir -p /home/$myuser/.vnc
		echo $mypasswd | vncpasswd -f > passwd
		sudo mv passwd /home/$myuser/.vnc/passwd
		sudo chown -R $myuser:$myuser /home/$myuser/.vnc
		sudo chmod 0600 /home/$myuser/.vnc/passwd
		# thx https://askubuntu.com/questions/328240/assign-vnc-password-using-script
		echo " VNC Password: $mypasswd"
		echo " copying configuration files"
		echo " vnc"
		sudo cp vnc/xstartup /home/$myuser/.vnc/xstartup
		sudo chmod +x /home/$myuser/.vnc/xstartup
		echo " config"
		sudo mkdir -p /home/$myuser/.config/
		sudo cp -r config/* /home/$myuser/.config/
		sudo chown -R $myuser:$myuser /home/$myuser/.config/
		echo " arduino"
		sudo mkdir -p /home/$myuser/.arduino15/
		sudo cp -r arduino15/* /home/$myuser/.arduino15/
		sudo chown -R $myuser:$myuser /home/$myuser/.arduino15/
	done


# setup vnc
	echo "Setting up VNC Server."

	echo " Checking for autostart service."
	VNC=""
	for i in $(seq 1 $vncCount)
	do
		VNC=$VNC$(systemctl status vncserver$i)
	done

	if [ -z "$VNC" \
		 -o -n "$(echo $VNC | grep "not found")" \
		 -o -n "$(echo $VNC | grep "Active: failed")" ] # empty -> could not be found
	then
		echo " Creating autostart service."
		
		for i in $(seq 1 $vncCount)
		do
			cat vnc/vncserver.service | \
			sed -e "s/USER/$vncBaseName$i/"  | \
			sed -e "s/NUM/$i/"               > vncserver$i.service

			sudo mv vncserver$i.service /etc/systemd/system/vncserver$i.service
		done
		sudo systemctl daemon-reload
	else
		echo " Autostart service already present."
	fi

	echo " Testing service:"
	for i in $(seq 1 $vncCount)
	do
		sudo systemctl start vncserver$i
	done
	VNC=$(systemctl status vncserver1 | grep -e "Active: active (running)")
	if [ "$VNC" ]
	then
		echo " Sucessful, enabling service!"
		for i in $(seq 1 $vncCount)
		do
			sudo systemctl enable vncserver$i
		done
	else
		echo "!Something went wrong! See status:"
		systemctl status vncserver1 | cat
	fi




# Enable I2C
	echo "Checking I2C"
	for i in 0 1
	do
		I2C=$(sudo raspi-config nonint get_i2c)
		if [ $I2C -eq 1 ] # if I2C is off
		then
			echo " Turning on I2C."
			a=$(sudo raspi-config nonint do_i2c 0 2>&1)
			# if the i2c driver was updated a reboot is necessary.
			# "modprobe: FATAL: Module i2c-dev not found in directory /lib/modules/5.15.32-v7l+"
			if echo $a | grep -i 'modprobe\|FATAL\|not found' > /dev/null
			then
				pleaseReboot=true
			fi

		else
			echo " I2C is active."
			break
		fi
	done 


# Create Shortcuts
	echo "Creating desktop and start menu shortcuts for ArduinoControl."

	wget -qO- http://letsgoing.org/img/personen/SMack.jpg >  ArduinoControl.jpg
	sudo mv ArduinoControl.jpg /usr/share/pixmaps/ArduinoControl.jpg

	#start menu
	sudo cp desktop-shortcuts/arduinocontrol.desktop /usr/share/applications/
	
	#desktops
	for i in $(seq 1 $vncCount)
	do
		sudo mkdir -p /home/$vncBaseName$i/Desktop/
		sudo cp desktop-shortcuts/arduinocontrol.desktop /home/$vncBaseName$i/Desktop/
	done
	
	echo "Creating desktop shortcuts for Arduino IDE."
	for i in $(seq 1 $vncCount)
	do
		sudo cp /usr/share/applications/arduino.desktop /home/$vncBaseName$i/Desktop/
	done

	echo "Adding example Sketches."
	for i in $(seq 1 $vncCount)
	do
		sudo mkdir -p /home/$vncBaseName$i/Arduino
		sudo cp -r Arduino/* /home/$vncBaseName$i/Arduino/
		sudo chown -R $vncBaseName$i:$vncBaseName$i /home/$vncBaseName$i/Arduino/
	done

#display useful info
	echo "Setup complete."
	echo "-------------------------------------------------"
	echo "Hostname:  $(hostname)"
	echo "IP-Adress: $(hostname -I | awk '{print $1}')"
	echo "-------------------------------------------------"

#display reboot message if necessary
	if [ "$pleaseReboot" = true ]
	then
		echo "PLEASE REBOOT (sudo reboot)"
	fi


# Camera Stuff...
#python3 -m pip install --upgrade --force-reinstall Pillow
# https://gist.github.com/melissacoleman/8e6bf42089ccb2a5ee60c0bea26c2061
# sudo userdel -r -f pi5
