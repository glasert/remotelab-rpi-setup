unsigned long nextSerial = 0;
const unsigned long serialDelay = 100;//ms
unsigned long lastToggle = 0;
bool blinkState = false;

void setup() {
  // put your setup code here, to run once:
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, INPUT);
  pinMode(7, INPUT);
  pinMode(8, INPUT);
  pinMode(12, INPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(A0, INPUT);

  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(2, digitalRead(6));
  digitalWrite(3, digitalRead(7));
  digitalWrite(4, digitalRead(8));
  digitalWrite(5, digitalRead(12));

  if (millis() >= nextSerial) {
    nextSerial = millis() + serialDelay;
    Serial.print("A0: ");
    Serial.println(analogRead(A0));
  }

  unsigned long delay = map(analogRead(A0), 0, 1023, 500, 5);
  if (millis() >= lastToggle + delay) {
    lastToggle = millis() + delay;
    blinkState = !blinkState;
    digitalWrite(LED_BUILTIN, blinkState);
  }
}
